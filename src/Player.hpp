#pragma once

#include "GameObject.hpp"
#include "engine/Animation.hpp"

#include <array>
#include <box2d/box2d.h>
#include <jngl.hpp>
#include <memory>

class b2World;
class Control;

class Player : public GameObject {
public:
	Player(b2World& world, const jngl::Vec2 position, const int playerNr);
	~Player();

	Player(const Player&) = delete;
	Player& operator=(const Player&) = delete;
	Player(Player&&) = delete;
	Player& operator=(Player&&) = delete;

	bool step() override;

	void dash(jngl::Vec2);
	void shoot();

	void draw() const override;
	void drawReflection() const override;

	void onContact(GameObject*) override;

	void createFixtureFromShape(const b2Shape& shape);
	bool isRepaired() const;
	bool isAlive() const;

	/// Der Spieler hat alles eingesammelt und kann jetzt töten
	bool isKing() const;
	void setKing(bool);

	void vibrate();

	int shield_up_time = 0;
	int points = 0;

private:
	jngl::Sprite sprite;
	jngl::Sprite spriteStunned;
	jngl::Sprite shadow;
	jngl::Sprite idle;
	jngl::Sprite crown;
	Animation walk;
	Animation animation_shield;
	Animation animation_attack;
	bool king = false;

	const int playerNr;

	// wird ungleichmäßig hochgezählt für die Animation des Kopfs
	float time = 0;

	int dashCountdown = 0;
	b2Vec2 dashDirection;

	int stun_time = 0;
	bool shield_active = false;
	bool has_punched = false;

	std::unique_ptr<Control> control;

	bool alive = true;
};
