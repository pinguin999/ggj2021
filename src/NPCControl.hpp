#pragma once

#include <jngl/Vec2.hpp>
#include "WorldTile.hpp"
#include "WorldMap.hpp"

class NPCControl {
public:
	NPCControl() = default;
	void step(std::shared_ptr<WorldMap>& worldMap, std::shared_ptr<WorldTile>& currentTile,
	          jngl::Vec2 currentPosition);
	jngl::Vec2 getMovement() const;

private:
	std::shared_ptr<WorldTile> targetTile;
	jngl::Vec2 targetPosition;
	jngl::Vec2 direction;
	float movementSpeed;

	std::shared_ptr<WorldTile> findTargetTile(std::shared_ptr<WorldMap>& worldMap,
	                                          std::shared_ptr<WorldTile>& currentTile) const;
};
