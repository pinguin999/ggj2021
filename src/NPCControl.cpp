#include "NPCControl.hpp"

void NPCControl::step(std::shared_ptr<WorldMap>& worldMap, std::shared_ptr<WorldTile>& currentTile,
                      jngl::Vec2 currentPosition) {

	if (!targetTile ||
	    (currentTile == targetTile &&
	     sqrt(pow(currentPosition.x - targetPosition.x, 2) +
	          pow(currentPosition.y - targetPosition.y, 2)) < (targetTile->getApothem() / 2))) {
		targetTile = findTargetTile(worldMap, currentTile);

		if (!targetTile) {
			direction.x = 0;
			direction.y = 0;
			movementSpeed = 0;
			return;
		}

		targetPosition.x = targetTile->getCenter().x +
		                   ((rand() / double(RAND_MAX) * 2 - 1.0) * targetTile->getApothem() / 2);
		targetPosition.y = targetTile->getCenter().y +
		                   ((rand() / double(RAND_MAX) * 2 - 1.0) * targetTile->getApothem() / 2);
		movementSpeed = (rand() / double(RAND_MAX)) * 0.002 + 0.001;
	}

	jngl::Vec2 directionToTarget = (targetPosition - currentPosition);
	boost::qvm::normalize(directionToTarget);
	direction = directionToTarget * movementSpeed;
}

jngl::Vec2 NPCControl::getMovement() const {
	return direction;
}

std::shared_ptr<WorldTile>
NPCControl::findTargetTile(std::shared_ptr<WorldMap>& worldMap,
                           std::shared_ptr<WorldTile>& currentTile) const {
	int newGridX = currentTile->getGridPosX() + rand() % 3 - 1;
	int newGridY = (newGridX == currentTile->getGridPosX())
	                   ? (currentTile->getGridPosY() + rand() % 3 - 1)
	                   : (currentTile->getGridPosY() + rand() % 1);
	auto newTile = worldMap->getTile(newGridX, newGridY);

	if (newTile == currentTile || newTile && newTile->getType() == WorldTile::Type::Water) {
		return std::shared_ptr<WorldTile>();
	}

	return newTile;
}
