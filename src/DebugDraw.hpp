#pragma once

#include "constants.hpp"
#include "jngl.hpp"

#include <box2d/box2d.h>

#ifdef NDEBUG
inline void DrawShape(jngl::Vec2 pos, b2Shape* shape) {}
inline void DrawShape(b2Body* body){}
#else
inline void DrawShape(jngl::Vec2 pos, b2Shape* shape) {
    if(const auto circle = dynamic_cast<b2CircleShape*>(shape)) {
        jngl::setColor(255,0,0, 150);
        jngl::drawCircle(pos, circle->m_radius * PIXEL_PER_METER);
    }
}

inline void DrawShape(b2Body* body) {
	b2Fixture* fixture = body->GetFixtureList();
	DrawShape(meterToPixel(body->GetPosition()), fixture->GetShape());
}

#endif
