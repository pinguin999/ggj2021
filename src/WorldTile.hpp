#pragma once

#include <jngl.hpp>

class WorldTile {
public:
	enum class Type { Ice, Water };

	WorldTile(Type type, int gridPosX, int gridPosY, float sideLength, float apothem,
	          jngl::Vec2 gridOrigin, std::string tileName, std::string tileAsset = "");

	void step();
	void draw() const;

	jngl::Vec2 getCenter() const;
	jngl::Vec2 getOrigin() const;
	float getApothem() const;
	float getWidth() const;
	float getHeight() const;
	Type getType() const;

	int getGridPosX() const;
	int getGridPosY() const;

private:
	Type type;
	int gridPosX;
	int gridPosY;
	jngl::Vec2 center;
	float sideLength;
	float apothem;
	std::vector<jngl::Vec2> vertices;
	std::string tileName;
    jngl::Vec2 assetPos;
    std::string tileAsset;
};
