#pragma once

#include "GameObject.hpp"
#include "NPCControl.hpp"
#include "engine/Animation.hpp"
#include "spine/AnimationState.h"
#include "WorldMap.hpp"

#include <jngl/work.hpp>
#include <string>
#include <array>
#include <box2d/box2d.h>
#include <jngl.hpp>
#include <memory>
#include <map>

struct spSkeleton;
struct spAnimationState;
struct spEvent;

namespace spine {
class SkeletonDrawable;
}

class SpineNPC : public GameObject{ //, public jngl::Work {
public:
	SpineNPC(b2World& world, std::shared_ptr<WorldMap> worldMap, const jngl::Vec2 position, const bool mate);
	~SpineNPC();
	bool step() override;
	void draw() const override;
    void setAnimation(std::string mood);
    bool mate = false;
    std::map<std::string, std::string> look;
    void gen_look();
    void set_look(std::map<std::string, std::string> look);

private:
	std::unique_ptr<spine::SkeletonDrawable> skeleton;
    spSkeletonData* skeletonData = nullptr;

    // spSkeleton** skeleton;
    spAnimationState* state = nullptr;
    double timeScale = 0.0;
    std::string* skin = nullptr;

	int blink = 255;
    static void animationStateListener(spAnimationState* state, spEventType type, spTrackEntry* entry, spEvent* event);
    static bool finished;

    std::shared_ptr<WorldMap> worldMap;
    std::unique_ptr<NPCControl> control;
    std::string currentAnimation;
};
