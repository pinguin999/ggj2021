#pragma once

#include "GameObject.hpp"
#include "engine/Animation.hpp"
#include "spine/AnimationState.h"

#include <jngl/work.hpp>
#include <string>
#include <array>
#include <box2d/box2d.h>
#include <jngl.hpp>
#include <memory>
#include <map>
#include "Control.hpp"
#include "WorldMap.hpp"

struct spSkeleton;
struct spAnimationState;
struct spEvent;

namespace spine {
class SkeletonDrawable;
}

class SpinePlayer : public GameObject{ //, public jngl::Work {
public:
	SpinePlayer(b2World& world, std::shared_ptr<WorldMap> worldMap, const jngl::Vec2 position, const int playerNr);
	~SpinePlayer();
	bool step() override;
	void draw() const override;
    std::map<std::string, std::string> look;
    void gen_look();

private:
	bool search();
    int cooldown = 0;
	std::unique_ptr<spine::SkeletonDrawable> skeleton;
    spSkeletonData* skeletonData = nullptr;

    // spSkeleton** skeleton;
    spAnimationState* state = nullptr;
    double timeScale = 0.0;
    std::string* skin = nullptr;

	int blink = 255;
    static void animationStateListener(spAnimationState* state, spEventType type, spTrackEntry* entry, spEvent* event);
    static bool finished;
    std::unique_ptr<Control> control;
    std::shared_ptr<WorldMap> worldMap;
	std::string currentAnimation;
    const jngl::Vec2 initialPosition;
    jngl::Vec2 deathPosition;
};
