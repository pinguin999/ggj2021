#pragma once

#include "ContactListener.hpp"
#include "WorldTile.hpp"
#include "WorldMap.hpp"

#include <jngl.hpp>
#include <set>
#include <vector>

class Animation;
class GameObject;
class Player;

class Game : public jngl::Work {
public:
	Game();
	~Game();
	void onLoad() override;
	void step() override;
	void draw() const override;
	int playersAlive() const;

private:
	b2World world;

	std::vector<std::unique_ptr<Animation>> animations;

	std::shared_ptr<WorldMap> worldMap;

	ContactListener contactListener;

	// Wenn 0, dann spawnt ein neues Fragment
	int spawnCountdown = 10;

	/// Wird auf den Gewinner gesetzt, sobald dies der Fall ist
	GameObject* playerWon = nullptr;

	/// Zeigt auf einen Spieler der am Leben ist
	Player* possibleWinner = nullptr;

	int playersAliveCount = 0;

	jngl::Sprite water{"water"};

#ifndef NDEBUG
	double cameraScale = 0;
#endif
};
