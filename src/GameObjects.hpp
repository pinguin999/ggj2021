#pragma once

#include <jngl.hpp>
#include "engine/Singleton.hpp"
#include "SpineNPC.hpp"

class GameObject;

class GameObjects : public Singleton<GameObjects>{
public:
	GameObjects();

	std::vector<std::shared_ptr<GameObject>> gameObjects;
private:

};
