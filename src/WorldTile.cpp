#include "WorldTile.hpp"

WorldTile::WorldTile(WorldTile::Type type, int gridPosX, int gridPosY, float sideLength,
                     float apothem, jngl::Vec2 gridOrigin, std::string tileName, std::string tileAsset)
: type(type), gridPosX(gridPosX), gridPosY(gridPosY), sideLength(sideLength), apothem(apothem),
  tileName(std::move(tileName)), tileAsset(std::move(tileAsset)) {
	center = jngl::Vec2(
	    (gridOrigin.x + sideLength + (gridPosX * ((3 * sideLength) / 2))),
	    (gridOrigin.y + apothem + ((gridPosX % 2) * apothem) + (2 * gridPosY * apothem)));

    assetPos.x = getCenter().x + ((rand() / double(RAND_MAX) * 2 - 1.0) * (getApothem() / 2));
    assetPos.y = getCenter().y + ((rand() / double(RAND_MAX) * 2 - 1.0) * (getApothem()) / 2);

	vertices.emplace_back(jngl::Vec2(center.x + (sideLength / 2), center.y - apothem));
	vertices.emplace_back(jngl::Vec2(center.x + sideLength, center.y));
	vertices.emplace_back(jngl::Vec2(center.x + (sideLength / 2), center.y + apothem));
	vertices.emplace_back(jngl::Vec2(center.x - (sideLength / 2), center.y + apothem));
	vertices.emplace_back(jngl::Vec2(center.x - sideLength, center.y));
	vertices.emplace_back(jngl::Vec2(center.x - (sideLength / 2), center.y - apothem));
}

void WorldTile::step() {
}

void WorldTile::draw() const {
	if (type != WorldTile::Type::Ice) {
		return;
	}

	jngl::pushMatrix();
	jngl::translate(getOrigin());
	int spriteWidth = jngl::getWidth(tileName);
	int spriteHeight = jngl::getHeight(tileName);
	float breakingEdgeSizePercentage = 0.45; // Edge height compared to ice floe height
	jngl::scale(getWidth() / spriteWidth,
	            (getHeight() + getHeight() * breakingEdgeSizePercentage) / spriteHeight);
	jngl::draw(tileName, 0, 0);
	jngl::popMatrix();

	if (!tileAsset.empty()) {
		jngl::pushMatrix();
		jngl::translate(assetPos);
        jngl::scale(0.15);
		jngl::draw(tileAsset, 0, 0);
		jngl::popMatrix();
	}

	/*    for (int i = 0; i < vertices.size(); ++i) {
	        jngl::setColor(255, 0, 0);
	        jngl::drawLine(vertices.at(i), vertices.at((i + 1) % vertices.size()));
	    }*/
}

jngl::Vec2 WorldTile::getCenter() const {
	return center;
}

jngl::Vec2 WorldTile::getOrigin() const {
	return jngl::Vec2(getCenter().x - sideLength, getCenter().y - apothem);
}

float WorldTile::getApothem() const {
	return apothem;
}

float WorldTile::getWidth() const {
	return sideLength * 2;
}

float WorldTile::getHeight() const {
	return apothem * 2;
}

WorldTile::Type WorldTile::getType() const {
	return type;
}

int WorldTile::getGridPosX() const {
	return gridPosX;
}

int WorldTile::getGridPosY() const {
	return gridPosY;
}
