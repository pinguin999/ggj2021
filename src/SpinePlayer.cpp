#include "SpinePlayer.hpp"

#include "engine/SkeletonDrawable.hpp"
#include "Game.hpp"
#include "constants.hpp"
#include "DebugDraw.hpp"
#include "Keyboard.hpp"
#include "jngl/matrix.hpp"
#include "camera.hpp"
#include "GameObjects.hpp"
#include "SpineNPC.hpp"

bool SpinePlayer::finished = false;

void SpinePlayer::animationStateListener(spAnimationState* state, spEventType type,
                                        spTrackEntry* entry, spEvent* event) {
	finished = false;

	switch (type) {
	case SP_ANIMATION_INTERRUPT:
		break;

	default:
		break;
	}
}

SpinePlayer::SpinePlayer(b2World& world, std::shared_ptr<WorldMap> worldMap, const jngl::Vec2 position, const int playerNr): worldMap(std::move(worldMap)), initialPosition(position) {
	b2BodyDef bodyDef;
	bodyDef.position = pixelToMeter(position);
	bodyDef.type = b2_dynamicBody;
	body = world.CreateBody(&bodyDef);
	body->SetGravityScale(0);
	body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
	body->SetLinearDamping(10.f);

	b2CircleShape shape = b2CircleShape();
	shape.m_radius = 12 / PIXEL_PER_METER;
	createFixtureFromShape(shape);

	// Hier die Spine JSON laden und dann:
	//
	std::string skeletonFile = "pinguin/pinguin4/pinguin.json";
	spAtlas* atlas = spAtlas_createFromFile("pinguin/pinguin4/pinguin.atlas", 0);
	spSkeletonJson* json = spSkeletonJson_create(atlas);

	skeletonData = spSkeletonJson_readSkeletonDataFile(json, (skeletonFile).c_str());
	if (!skeletonData) {
		// this->loading = RESOURCE_FAILED_LOADING;
		throw std::runtime_error("Could not parse JSON " + skeletonFile);
	}
	spSkeletonJson_dispose(json);

	spAnimationStateData* animationStateData = spAnimationStateData_create(skeletonData);

	skeleton = std::make_unique<spine::SkeletonDrawable>(skeletonData, animationStateData);
    currentAnimation = "idle";
    spAnimationState_setAnimationByName(skeleton->state, 0, "respawnVar", false);
	spAnimationState_addAnimationByName(skeleton->state, 0, "whereismymate", true, 1);

	skeleton->state->listener = (spAnimationStateListener) & this->animationStateListener;

	spSkeleton_setAttachment(skeleton->skeleton, "hat", nullptr);

	skeleton->step();

	control = std::make_unique<Keyboard>(0);
}

SpinePlayer::~SpinePlayer() = default;

void SpinePlayer::gen_look() {
    look.clear();
    int random_variable = std::rand() % 6;
    std::string hat = "";
    if (random_variable < 5) {
        char variant = 'A' + (char)random_variable;
        hat = std::string("hat_") + variant;
    } else {
        spSkeleton_setAttachment(skeleton->skeleton, "forhead", nullptr);
        look.insert(std::pair<std::string, std::string>("forhead", ""));
    }
    spSkeleton_setAttachment(skeleton->skeleton, "hat", hat.empty() ? nullptr : hat.c_str());
    look.insert(std::pair<std::string, std::string>("hat", hat));

    {
        char variant = 'A' + (char)(std::rand() % 3);
        std::string model = std::string("body_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "body", model.c_str());
        look.insert(std::pair<std::string, std::string>("body", model));
    }

    {
        char variant = 'A' + (char)(std::rand() % 2);
        std::string model = std::string("armL_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "armL", model.c_str());
        look.insert(std::pair<std::string, std::string>("armL", model));
    }

    {
        char variant = 'A' + (char)(std::rand() % 2);
        std::string model = std::string("armR_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "armR", model.c_str());
        look.insert(std::pair<std::string, std::string>("armR", model));
    }

    {
        char variant = 'A' + (char)(std::rand() % 2);
        std::string modelL = std::string("eyeL_") + variant;
        std::string modelR = std::string("eyeR_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "eyeL", modelL.c_str());
        look.insert(std::pair<std::string, std::string>("eyeL", modelL));
        spSkeleton_setAttachment(skeleton->skeleton, "eyeR", modelR.c_str());
        look.insert(std::pair<std::string, std::string>("eyeR", modelR));
    }

    {
        char variant = 'A' + (char)(std::rand() % 2);
        std::string model = std::string("head_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "head", model.c_str());
        look.insert(std::pair<std::string, std::string>("head", model));
    }

    {
        char variant = 'A' + (char)(std::rand() % 2);
        std::string model = std::string("neck_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "neck", model.c_str());
        look.insert(std::pair<std::string, std::string>("neck", model));
    }

    {
        char variant = 'A' + (char)(std::rand() % 3);
        std::string model = std::string("peak_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "peak", model.c_str());
        look.insert(std::pair<std::string, std::string>("peak", model));
    }

    {
        char variant = 'A' + (char)(std::rand() % 2);
        std::string model = std::string("cheeks_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "cheeks", model.c_str());
        look.insert(std::pair<std::string, std::string>("cheeks", model));
    }

    {
        char variant = 'A' + (char)(std::rand() % 2);
        std::string model = std::string("wristR_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "wristR", model.c_str());
        look.insert(std::pair<std::string, std::string>("wristR", model));
    }
}

bool SpinePlayer::step() {
	if (!skeleton || finished) {
		jngl::setWork(std::make_shared<Game>());
		return false;
	}
	skeleton->step();
    Camera* cam = Camera::handle();


	if (currentAnimation == "respawnVar") {
        auto anim = spAnimationState_getCurrent(skeleton->state, 0);
        auto camPos = deathPosition + std::min(anim->trackTime / (anim->animationEnd / 4), 1.f)  * (initialPosition - deathPosition);
        cam->position = camPos;
        if (anim->trackTime >= anim->animationEnd) {
            currentAnimation = "idle";
            spAnimationState_setAnimationByName(skeleton->state, 0, currentAnimation.c_str(), true);
        }
		return false;
	}

    cam->position = meterToPixel(body->GetPosition());

    auto currentPos = meterToPixel(body->GetPosition());
    auto currentTile = worldMap->getTileAt(currentPos);
    bool touchesWater = currentTile && currentTile->getType() == WorldTile::Type::Water;
    if (touchesWater) {
		if (currentAnimation != "death") {
            jngl::play("sfx/splash.ogg");
			auto fallDirection = currentTile->getCenter() - currentPos;
			body->SetLinearVelocity(pixelToMeter(fallDirection * 10));
			currentAnimation = "death";
			spAnimationState_setAnimationByName(skeleton->state, 0, currentAnimation.c_str(), false);
		}
        auto anim = spAnimationState_getCurrent(skeleton->state, 0);
		if (anim->trackTime >= anim->animationEnd) {
            body->SetLinearVelocity(b2Vec2(0,0));
            body->SetTransform(pixelToMeter(initialPosition),body->GetAngle());
            currentAnimation = "respawnVar";
            skeleton->skeleton->scaleX = 1;
			deathPosition = currentPos;
            spAnimationState_setAnimationByName(skeleton->state, 0, "respawnVar", false);
		}
		return false;
    }


    std::string anim = "idle";
	float linX = abs(body->GetLinearVelocity().x);
	float linY = abs(body->GetLinearVelocity().y);
    if (linX > 0.01 || linY > 0.01) {
		anim = "walk";
	}
    if (anim != currentAnimation) {
        currentAnimation = anim;
        spAnimationState_setAnimationByName(skeleton->state, 0, currentAnimation.c_str(), true);
    }

    if (abs(body->GetLinearVelocity().x) > 0.01) {
        if (body->GetLinearVelocity().x > 0) {
            skeleton->skeleton->scaleX = 1;
        } else {
            skeleton->skeleton->scaleX = -1;
        }
    }
	cooldown = cooldown -1;
	if (control->dash())
	{
		if(cooldown<=0){
			cooldown = 60;
			search();
		}

	}

    jngl::Vec2 movement = control->getMovement() * 0.8;
    body->SetLinearVelocity(b2Vec2(movement.x, movement.y));
	return false;
}

void SpinePlayer::draw() const {
	jngl::pushMatrix();
	// DEBUG
	// DrawShape(body);
	jngl::translate(meterToPixel(body->GetPosition()));
	jngl::scale(0.05);
	skeleton->draw();
	jngl::popMatrix();
}

bool SpinePlayer::search() {
	int notice_range = 50;
	spAnimationState_setAnimationByName(skeleton->state, 0, "nootnoot", false);
	jngl::play("sfx/pinguask.ogg");
	spAnimationState_addAnimationByName(skeleton->state, 0, "walk", true, 1);
	for (auto pingu : GameObjects::handle()->gameObjects){
		if (const auto npc = dynamic_cast<SpineNPC*>(pingu.get())) {
			jngl::Vec2 pos = npc->getPosition();
			if (pos.x > this->getPosition().x - notice_range &&  pos.x < this->getPosition().x + notice_range)
			{
				if (pos.y > this->getPosition().y - notice_range &&  pos.y < this->getPosition().y + notice_range)
				{
					if(npc->mate){
						npc->setAnimation("happy");
						jngl::play("sfx/pingufound.ogg");
					}else{
						npc->setAnimation("nope");
						jngl::play("sfx/pinguwrong.ogg");
					}
				}
			}
		}
	}
	return false;
}
