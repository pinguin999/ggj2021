#include "IntroMovie.hpp"

#include "engine/SkeletonDrawable.hpp"
#include "Game.hpp"

bool IntroMovie::finished = false;

void IntroMovie::animationStateListener(spAnimationState* state, spEventType type,
                                        spTrackEntry* entry, spEvent* event) {
	finished = true;

	switch (type) {
	case SP_ANIMATION_INTERRUPT:
		break;

	default:
		break;
	}
}

IntroMovie::IntroMovie() {
	// Hier die Spine JSON laden und dann:
	//
	std::string skeletonFile = "intro/pingu.json";
	spAtlas* atlas = spAtlas_createFromFile("intro/pingu.atlas", 0);
	jngl::play("sfx/intro_song.ogg");
	spSkeletonJson* json = spSkeletonJson_create(atlas);
	json->scale = 0.62;

	skeletonData = spSkeletonJson_readSkeletonDataFile(json, (skeletonFile).c_str());
	if (!skeletonData) {
		// this->loading = RESOURCE_FAILED_LOADING;
		throw std::runtime_error("Could not parse JSON " + skeletonFile);
	}
	spSkeletonJson_dispose(json);

	spAnimationStateData* animationStateData = spAnimationStateData_create(skeletonData);

	skeleton = std::make_unique<spine::SkeletonDrawable>(skeletonData, animationStateData);
	spAnimationState_setAnimationByName(skeleton->state, 0, "Intro", false);
	skeleton->state->listener = (spAnimationStateListener) & this->animationStateListener;

	skeleton->step();
}

IntroMovie::~IntroMovie() = default;

void IntroMovie::step() {
	if (!skeleton || finished) {
		jngl::setWork(std::make_shared<Game>());
		return;
	}
	skeleton->step();
}

void IntroMovie::draw() const {
	jngl::pushMatrix();
	jngl::scale(0.55);
	jngl::translate(0, 0);
	skeleton->draw();
	jngl::popMatrix();
}
