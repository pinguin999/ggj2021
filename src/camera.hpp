#pragma once

#include <jngl.hpp>
#include "engine/Singleton.hpp"

class Camera : public Singleton<Camera>{
public:
	Camera();

	jngl::Vec2 position = jngl::Vec2(0,0);
private:

};
