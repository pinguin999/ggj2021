#pragma once

#include <jngl.hpp>

#include "WorldTile.hpp"

class WorldMap {

public:
	WorldMap(jngl::Vec2 size, jngl::Vec2 origin, int tileCountX, int tileCountY);

	std::vector<std::vector<std::shared_ptr<WorldTile>>> getTiles() const;

	void step();
	void draw() const;

	std::shared_ptr<WorldTile> getTile(int gridX, int gridY) const;
	std::shared_ptr<WorldTile> getTileAt(jngl::Vec2 pos) const;

private:
	jngl::Vec2 size;
	jngl::Vec2 origin;
	float sideLength;
	float apothem;
	std::vector<std::vector<std::shared_ptr<WorldTile>>> tiles;
};
