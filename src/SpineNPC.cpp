#include "SpineNPC.hpp"

#include "engine/SkeletonDrawable.hpp"
#include "Game.hpp"
#include "constants.hpp"
#include "DebugDraw.hpp"
#include "jngl/matrix.hpp"

bool SpineNPC::finished = false;

void SpineNPC::animationStateListener(spAnimationState* state, spEventType type,
                                        spTrackEntry* entry, spEvent* event) {
	finished = false;

	switch (type) {
	case SP_ANIMATION_INTERRUPT:
		break;

	default:
		break;
	}
}

SpineNPC::SpineNPC(b2World& world, std::shared_ptr<WorldMap> worldMap, const jngl::Vec2 position, const bool mate): worldMap(std::move(worldMap)) {
	b2BodyDef bodyDef;
	this->mate = mate;
	bodyDef.position = pixelToMeter(position);
	bodyDef.type = b2_dynamicBody;
	body = world.CreateBody(&bodyDef);
	body->SetGravityScale(0);
	body->GetUserData().pointer = reinterpret_cast<uintptr_t>(static_cast<GameObject*>(this));
	body->SetLinearDamping(5 + rand() % 10);

	b2CircleShape shape = b2CircleShape();
	shape.m_radius = 12 / PIXEL_PER_METER;
	createFixtureFromShape(shape);

	// Hier die Spine JSON laden und dann:
	//
	std::string skeletonFile = "pinguin/pinguin4/pinguin.json";
	spAtlas* atlas = spAtlas_createFromFile("pinguin/pinguin4/pinguin.atlas", 0);
	spSkeletonJson* json = spSkeletonJson_create(atlas);

	skeletonData = spSkeletonJson_readSkeletonDataFile(json, (skeletonFile).c_str());
	if (!skeletonData) {
		// this->loading = RESOURCE_FAILED_LOADING;
		throw std::runtime_error("Could not parse JSON " + skeletonFile);
	}
	spSkeletonJson_dispose(json);

	spAnimationStateData* animationStateData = spAnimationStateData_create(skeletonData);

	skeleton = std::make_unique<spine::SkeletonDrawable>(skeletonData, animationStateData);
    currentAnimation = "idle";
	spAnimationState_setAnimationByName(skeleton->state, 0, currentAnimation.c_str(), true);
	skeleton->state->listener = (spAnimationStateListener) & this->animationStateListener;

	skeleton->step();

    control = std::make_unique<NPCControl>();
}

SpineNPC::~SpineNPC() {
    body->GetWorld()->DestroyBody(body);
};

void SpineNPC::gen_look() {
	look.clear();
    int random_variable = std::rand() % 6;
    std::string hat = "";
    if (random_variable < 5) {
        char variant = 'A' + (char)random_variable;
        hat = std::string("hat_") + variant;
    } else {
        spSkeleton_setAttachment(skeleton->skeleton, "forhead", nullptr);
        look.insert(std::pair<std::string, std::string>("forhead", ""));
    }
    spSkeleton_setAttachment(skeleton->skeleton, "hat", hat.empty() ? nullptr : hat.c_str());
    look.insert(std::pair<std::string, std::string>("hat", hat));

    {
        char variant = 'A' + (char)(std::rand() % 3);
        std::string model = std::string("body_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "body", model.c_str());
        look.insert(std::pair<std::string, std::string>("body", model));
    }

    {
        char variant = 'A' + (char)(std::rand() % 2);
        std::string model = std::string("armL_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "armL", model.c_str());
        look.insert(std::pair<std::string, std::string>("armL", model));
    }

    {
        char variant = 'A' + (char)(std::rand() % 2);
        std::string model = std::string("armR_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "armR", model.c_str());
        look.insert(std::pair<std::string, std::string>("armR", model));
    }

    {
        char variant = 'A' + (char)(std::rand() % 2);
        std::string modelL = std::string("eyeL_") + variant;
        std::string modelR = std::string("eyeR_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "eyeL", modelL.c_str());
        look.insert(std::pair<std::string, std::string>("eyeL", modelL));
        spSkeleton_setAttachment(skeleton->skeleton, "eyeR", modelR.c_str());
        look.insert(std::pair<std::string, std::string>("eyeR", modelR));
    }

	{
		char variant = 'A' + (char)(std::rand() % 2);
		std::string model = std::string("head_") + variant;
		spSkeleton_setAttachment(skeleton->skeleton, "head", model.c_str());
		look.insert(std::pair<std::string, std::string>("head", model));
	}

    {
        char variant = 'A' + (char)(std::rand() % 2);
        std::string model = std::string("neck_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "neck", model.c_str());
        look.insert(std::pair<std::string, std::string>("neck", model));
    }

    {
        char variant = 'A' + (char)(std::rand() % 3);
        std::string model = std::string("peak_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "peak", model.c_str());
        look.insert(std::pair<std::string, std::string>("peak", model));
    }

    {
        char variant = 'A' + (char)(std::rand() % 2);
        std::string model = std::string("cheeks_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "cheeks", model.c_str());
        look.insert(std::pair<std::string, std::string>("cheeks", model));
    }

    {
        char variant = 'A' + (char)(std::rand() % 2);
        std::string model = std::string("wristR_") + variant;
        spSkeleton_setAttachment(skeleton->skeleton, "wristR", model.c_str());
        look.insert(std::pair<std::string, std::string>("wristR", model));
    }
}

void SpineNPC::set_look(std::map<std::string, std::string> look){

	for (auto const& [key, val] : look)
	{
		if (val != std::string(""))
		{
			spSkeleton_setAttachment(skeleton->skeleton, key.c_str(), val.c_str());
		}else{
			spSkeleton_setAttachment(skeleton->skeleton, key.c_str(), nullptr);
		}
	}
}


bool SpineNPC::step() {
	if (!skeleton || finished) {
		jngl::setWork(std::make_shared<Game>());
		return false;
	}
    skeleton->step();

    int random_variable = std::rand();
	if (random_variable > RAND_MAX*0.9991)
	{
        random_variable = std::rand();
        if (random_variable > RAND_MAX/2)
	    {
        jngl::play("sfx/pinguwenk0.ogg");

        }else{
            random_variable = std::rand();
            if (random_variable > RAND_MAX/2)
	        {
                jngl::play("sfx/pinguwenk1.ogg");

            }else{
                jngl::play("sfx/pinguwenk2.ogg");
            }

        }

    }
	auto currentPos = meterToPixel(body->GetPosition());
    auto currentTile = worldMap->getTileAt(currentPos);
	control->step(worldMap, currentTile, currentPos);

    bool touchesWater = currentTile && currentTile->getType() == WorldTile::Type::Water;
    if (touchesWater) {
		if (!mate) {
            if (currentAnimation != "death")
            {
                jngl::play("sfx/splash.ogg");
                auto fallDirection = currentTile->getCenter() - currentPos;
                body->SetLinearVelocity(pixelToMeter(fallDirection * 10));
                currentAnimation = "death";
                spAnimationState_setAnimationByName(skeleton->state, 0, currentAnimation.c_str(), false);
            }
			auto anim = spAnimationState_getCurrent(skeleton->state, 0);
			if (anim->trackTime >= anim->animationEnd) {
				return true;
			}
			return false;
		}
    }

    std::string anim = "idle";
    float linX = abs(body->GetLinearVelocity().x);
    float linY = abs(body->GetLinearVelocity().y);
    if (linX > 0.4 && linY < 0.1) {
        anim = "slide";
    } else if (linX > 0.01 || linY > 0.01) {
        anim = "walk";
    }
    if (anim != currentAnimation) {
        currentAnimation = anim;
        spAnimationState_setAnimationByName(skeleton->state, 0, currentAnimation.c_str(), true);
    }

	if (abs(body->GetLinearVelocity().x) > 0.01) {
		if (body->GetLinearVelocity().x > 0) {
			skeleton->skeleton->scaleX = 1;
		} else {
			skeleton->skeleton->scaleX = -1;
		}
	}
    jngl::Vec2 movement = control->getMovement();
    //body->SetLinearVelocity(b2Vec2(movement.x, movement.y));
    body->ApplyLinearImpulse(b2Vec2(movement.x, movement.y), body->GetPosition(), true);

	return false;
}

void SpineNPC::draw() const {
	jngl::pushMatrix();
	// DEBUG
#ifdef NDEBUG
	if (mate)
		DrawShape(body);
#endif
	jngl::translate(meterToPixel(body->GetPosition()));
	jngl::scale(0.05);
	skeleton->draw();
	jngl::popMatrix();
}

void SpineNPC::setAnimation(std::string mood) {
	spAnimationState_setAnimationByName(skeleton->state, 0, mood.c_str(), true);
}
