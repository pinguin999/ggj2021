#include "WorldMap.hpp"

WorldMap::WorldMap(jngl::Vec2 size, jngl::Vec2 origin, int tileCountX, int tileCountY)
: size(size), origin(origin), sideLength(size.x / 3 * 2 / tileCountX),
  apothem(size.y / 2 / tileCountY) {
	for (int j = 0; j < tileCountY; j++) {
		std::vector<std::shared_ptr<WorldTile>> row;
		for (int i = 0; i < tileCountX; i++) {
			WorldTile::Type tileType;
			// Outside tiles are always water
			if (i == 0 || i == tileCountX - 1 || j == 0 || j == tileCountY - 1) {
				tileType = WorldTile::Type::Water;
			} else {
				tileType = rand() % 6 ? WorldTile::Type::Ice : WorldTile::Type::Water;
			}
			std::string scholle;
			float random_variable = std::rand() / double(RAND_MAX);
			if (random_variable > 0.75) {
                scholle = "scholle3" ;
			} else if (random_variable > 0.5) {
                scholle = "scholle2" ;
            } else if (random_variable > 0.25) {
                scholle = "scholle1" ;
            } else {
                scholle = "scholle0" ;
            }
            std::string asset;
			if (tileType == WorldTile::Type::Ice && rand() % 4 == 0) {
                int r = rand() % 3;
				if (r == 0) {
					asset = "poo";
				} else if (r == 1) {
					asset = "egg";
				} else {
					asset = "fisch";
				}
			}
			row.emplace_back(std::make_shared<WorldTile>(tileType, i, j, sideLength, apothem, origin, scholle, asset));
			
		}
		tiles.emplace_back(row);
	}
}

std::vector<std::vector<std::shared_ptr<WorldTile>>> WorldMap::getTiles() const {
	return tiles;
}

void WorldMap::step() {
	for (const auto& row : tiles) {
		for (const auto& tile : row) {
			tile->step();
		}
	}
}

void WorldMap::draw() const {
	for (const auto& row : tiles) {
		// First draw upper tiles in a row..
		for (int y = 0; y < row.size(); y += 2) {
			row.at(y)->draw();
		}
		// Then lower tiles
		for (int y = 1; y < row.size(); y += 2) {
			row.at(y)->draw();
		}
	}

	/*	// Draw map bounds
	    jngl::setColor(0, 0, 255);
	    jngl::drawLine(origin.x, origin.y, origin.x + size.x, origin.y);
	    jngl::drawLine(origin.x + size.x, origin.y, origin.x + size.x, origin.y + size.y);
	    jngl::drawLine(origin.x + size.x, origin.y + size.y, origin.x, origin.y + size.y);
	    jngl::drawLine(origin.x, origin.y + size.y, origin.x, origin.y);*/
}

std::shared_ptr<WorldTile> WorldMap::getTile(int gridX, int gridY) const {
    if (gridY < 0 || gridY > tiles.size() - 1) {
		return std::shared_ptr<WorldTile>();
	}

	auto row = tiles.at(gridY);

    if (gridX < 0 || gridX > row.size() - 1) {
        return std::shared_ptr<WorldTile>();
    }

	return row.at(gridX);
}

std::shared_ptr<WorldTile> WorldMap::getTileAt(jngl::Vec2 pos) const {
	int x = pos.x - origin.x;
	int y = pos.y - origin.y;

	float gridHeight = apothem * 2;
	float gridWidth = sideLength * 2 - sideLength / 2;

	int column = (int)(x / gridWidth);
	int row;

	bool columnIsOdd = column % 2 == 1;

	if (columnIsOdd) {
		row = (int)((y - apothem) / gridHeight);
	} else {
		row = y / gridHeight;
	}

	double relX = x - (column * gridWidth);
	double relY;

	if (columnIsOdd) {
		relY = (y - (row * gridHeight)) - apothem;
	} else {
		relY = y - (row * gridHeight);
	}

	float c = sideLength / 2;
	float m = c / apothem;

	if (relX < (-m * relY) + c) {
		--column;
		if (!columnIsOdd) {
			--row;
		}
	} else if (relX < (m * relY) - c) {
		--column;
		if (columnIsOdd) {
			++row;
		}
	}

	if (row >= 0 && row < tiles.size() && column >= 0 && column < tiles.at(row).size()) {
		return tiles.at(row).at(column);
	}

	return std::shared_ptr<WorldTile>();
}
