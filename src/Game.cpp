#include "Game.hpp"

#include "Box.hpp"
#include "constants.hpp"
#include "SpinePlayer.hpp"
#include "SpineNPC.hpp"
#include "WinningScreen.hpp"
#include "camera.hpp"
#include "GameObjects.hpp"

#include <cmath>
#include <map>

Game::Game()
: world({ 0, 0 /* gravity */ }) {

	world.SetContactListener(&contactListener);
	const int WORLDX = 1400;
	const int WORLDY = 1000;
	const int TILE_COUNT_X = 15;
	const int TILE_COUNT_Y = 15;
    worldMap = std::make_shared<WorldMap>(jngl::Vec2(WORLDX, WORLDY), jngl::Vec2(-WORLDX/2, -WORLDY/2), TILE_COUNT_X, TILE_COUNT_Y);

    // Add players pinguin
    std::shared_ptr<WorldTile> playerTile;
    do {
        auto tile = worldMap->getTile(1, rand() % TILE_COUNT_Y);
        if (tile->getType() == WorldTile::Type::Ice) {
            playerTile = tile;
        }
    } while (!playerTile);
    auto player = std::make_shared<SpinePlayer>(world, worldMap, playerTile->getCenter(), false);
    player->gen_look();
    GameObjects::handle()->gameObjects.emplace_back(player);

    // Add players mate
	std::shared_ptr<WorldTile> mateTile;
	do {
		auto tile = worldMap->getTile(rand() % TILE_COUNT_X, rand() % TILE_COUNT_Y);
		if (tile->getType() == WorldTile::Type::Ice && tile != playerTile) {
			mateTile = tile;
		}
	} while (!mateTile);
	auto mate = std::make_shared<SpineNPC>(world, worldMap, mateTile->getCenter(), true);
	mate->set_look(player->look);
	GameObjects::handle()->gameObjects.emplace_back(mate);


	// Add NPC penguins
	for (const auto& row : worldMap->getTiles()) {
		for (const auto& tile : row) {
			if (tile->getType() == WorldTile::Type::Ice && tile != mateTile && tile != playerTile) {
				bool placePenguin = rand() % 2 == 0;
				if (placePenguin) {
					auto npc = std::make_shared<SpineNPC>(world, worldMap, tile->getCenter(), false);
					npc->gen_look();
					while (npc->look == player->look)
					{
						npc->gen_look();
					}
					GameObjects::handle()->gameObjects.emplace_back(npc);
				}
			}
		}
	}
	jngl::loop("sfx/game_song.ogg");
}

Game::~Game() {
	GameObjects::handle()->gameObjects.clear();
}

void Game::step() {
	world.Step(playerWon ? (1.f / 120.f) : (1.f / 60.f), 8, 3);

	worldMap->step();

	playersAliveCount = 0;
	for (auto it = GameObjects::handle()->gameObjects.begin();
	     it != GameObjects::handle()->gameObjects.end(); ++it) {
		if ((*it)->step()) {
			it = GameObjects::handle()->gameObjects.erase(it);
			if (it == GameObjects::handle()->gameObjects.end()) {
				break;
			}
		}
	}
	for (auto it = animations.begin(); it != animations.end(); ++it) {
		if ((*it)->step()) {
			it = animations.erase(it);
			if (it == animations.end()) {
				break;
			}
		}
	}

#ifndef NDEBUG
	if (jngl::keyPressed(jngl::key::F5)) {
		jngl::setWork(std::make_shared<Game>());
	}
	cameraScale += jngl::getMouseWheel() / 10.;
#endif
}

void Game::draw() const {
#ifndef NDEBUG
	jngl::scale(std::exp(cameraScale));
#endif
	Camera* cam = Camera::handle();
	jngl::translate(-cam->position);

    jngl::pushMatrix();
    jngl::translate(800, 600);
    water.drawScaled(0.58, 0.58);
	jngl::popMatrix();
	worldMap->draw();

	std::multimap<double, GameObject*> orderedByZIndex;
	std::vector<const SpinePlayer*> players;
	for (const auto& obj : GameObjects::handle()->gameObjects) {
		orderedByZIndex.emplace(obj->getZIndex(), obj.get());
		if (const auto player = dynamic_cast<SpinePlayer*>(obj.get())) {
			players.emplace_back(player);
		}
	}

	for (const auto& gameObject : orderedByZIndex) {
		gameObject.second->draw();
	}

	for (auto& animation : animations) {
		animation->draw();
	}
}

int Game::playersAlive() const {
	return playersAliveCount;
}

void Game::onLoad() {
	// Wird z. B. aufgerufen, wenn wir vom WinningScreen wieder aktiv gesetzt wurden.
	playerWon = nullptr;
}
